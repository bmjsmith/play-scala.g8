addSbtPlugin("net.databinder.giter8" %% "giter8-plugin" % "0.4.5")

resolvers ++= Seq(
    "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
)
