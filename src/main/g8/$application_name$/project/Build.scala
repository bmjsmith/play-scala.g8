import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "$application_name$"
    val appVersion      = "$application_version$"
	
    val appDependencies = Nil
 
    val main = play.Project(
      appName, appVersion, appDependencies
    ) 

}
